package kolkokrzyzyk;

public class StanGry {
    
    public String pole1;
    public String pole2;
    public String pole3;
    public String pole4;
    public String pole5;
    public String pole6;
    public String pole7;
    public String pole8;
    public String pole9;
    public String komunikat;
    public boolean gracz;
    public int nrTury;
    
    public StanGry() {
        nowaGra();
    }
    
    public StanGry(StanGry sg2) {
        this.pole1 = sg2.pole1;
        this.pole2 = sg2.pole2;
        this.pole3 = sg2.pole3;
        this.pole4 = sg2.pole4;
        this.pole5 = sg2.pole5;
        this.pole6 = sg2.pole6;
        this.pole7 = sg2.pole7;
        this.pole8 = sg2.pole8;
        this.pole9 = sg2.pole9;
        this.komunikat = sg2.komunikat;
        this.gracz = sg2.gracz;
        this.nrTury = sg2.nrTury;
    }
    
    public void nowaGra() {
        pole1 = "";
        pole2 = "";
        pole3 = "";
        pole4 = "";
        pole5 = "";
        pole6 = "";
        pole7 = "";
        pole8 = "";
        pole9 = "";
        komunikat = "Ruch gracza: KÓŁKO";
        gracz = false;
        nrTury = 0;
    }
    
    public void ustawStanGry(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, boolean g) {
        pole1 = p1;
        pole2 = p2;
        pole3 = p3;
        pole4 = p4;
        pole5 = p5;
        pole6 = p6;
        pole7 = p7;
        pole8 = p8;
        pole9 = p9;
        gracz = g;
    }
    
    // 0 - grac dalej, 1 - wygrana O, 2 - wygrana X, 3 - remis
    public int sprawdzCzyKoniec() {
        if( pole1.equals("O") && pole2.equals("O") && pole3.equals("O") )
            return 1;
        else if( pole4.equals("O") && pole5.equals("O") && pole6.equals("O") )
            return 1;
        else if( pole7.equals("O") && pole8.equals("O") && pole9.equals("O") )
            return 1;
        else if( pole1.equals("O") && pole4.equals("O") && pole7.equals("O") )
            return 1;
        else if( pole2.equals("O") && pole5.equals("O") && pole8.equals("O") )
            return 1;
        else if( pole3.equals("O") && pole6.equals("O") && pole9.equals("O") )
            return 1;
        else if( pole1.equals("O") && pole5.equals("O") && pole9.equals("O") )
            return 1;
        else if( pole3.equals("O") && pole5.equals("O") && pole7.equals("O") )
            return 1;
        else if( pole1.equals("X") && pole2.equals("X") && pole3.equals("X") )
            return 2;
        else if( pole4.equals("X") && pole5.equals("X") && pole6.equals("X") )
            return 2;
        else if( pole7.equals("X") && pole8.equals("X") && pole9.equals("X") )
            return 2;
        else if( pole1.equals("X") && pole4.equals("X") && pole7.equals("X") )
            return 2;
        else if( pole2.equals("X") && pole5.equals("X") && pole8.equals("X") )
            return 2;
        else if( pole3.equals("X") && pole6.equals("X") && pole9.equals("X") )
            return 2;
        else if( pole1.equals("X") && pole5.equals("X") && pole9.equals("X") )
            return 2;
        else if( pole3.equals("X") && pole5.equals("X") && pole7.equals("X") )
            return 2;
        else if( nrTury == 9 )
            return 3;
        return 0;
    }
    
}
